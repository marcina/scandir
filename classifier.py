#! /usr/bin/env python
from sys import argv
import magic

filename = argv[1]

# - magic installed from Debian repos is not the same version as 
#   what is installed via pip. The API below works for system verison,
#   if the pip one is used, AttributeError will be thrown.
#
# magic.MAGIC_MIME_TYPE - only mime type
# magic.MAGIC_NONE      - all info (codepage,compression)
m = magic.open(magic.MAGIC_MIME_TYPE)
m.load()
print m.file(argv[1])
m.close()
